import React from 'react';
import {Logo} from '../../../../shared/components/Logo/';
import {FooterMenu} from '../../../../shared/components/FooterMenu/';
import {SocialIcons} from '../../../../shared/components/SocialIcons/';
import {Copyright} from '../../../../shared/components/Copyright/';
import './Footer.css';
export const Footer = (props) =>{
  const {logo, footerMenu} = props;
  return (
    <footer className='footer'>
      <div className='container'>
          <div className='footer-row'>
            <FooterMenu footerMenu={footerMenu}/>
            <Logo {...logo} type='color'/>
            <SocialIcons/>
          </div>
          <div className='footer-r'>
            <Copyright />
          </div>
      </div>
    </footer>
  );
};