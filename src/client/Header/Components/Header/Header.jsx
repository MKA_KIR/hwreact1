import React from 'react';
import {Navbar} from '../../../Navbar/Components/Navbar';
import {FilmLongInfo} from '../../../../shared/components/FilmLongInfo';
import image from '../../../../shared/components/FilmLongInfo/image/junglebook2@1X.png'
import './Header.css';


export const Header = (props) =>{

  const {navbar, filmLongInfo} = props;
  return (
    <header className ='header' style={{backgroundImage: `url(${image})`}}>
      <Navbar {...navbar} />
      <div className='container'>
        <div className='header-row'>
          <FilmLongInfo {...filmLongInfo}/>
        </div>
      </div>
    </header>
  );
};

