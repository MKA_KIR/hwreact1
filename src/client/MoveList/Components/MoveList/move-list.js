function importAll(r) {
  return r.keys().map(r);
}

const images = importAll(require.context('../../../../assets/images/', false, /\.(png|jpe?g|svg)$/));

export const moveList = {
  bannerRow: 3,
    moveCards: [
      {
        src: images[0] ,
        name: 'Fantastic Beasts...',
        rating: 4.7,
        category: ['Adventure', 'Family', 'Fantasy'],
      },
      {
        src: images[1] ,
        name: 'Alice Through th...',
        rating: 4.1,
        category: ['Adventure', 'Family', 'Fantasy'],
      },
      {
        src: images[2] ,
        name: 'AssAssin’s Creed',
        rating: 4.2,
        category: ['Action, Adventure, Fantasy'],
      },
      {
        src: images[3] ,
        name: 'Captain America...',
        rating: 4.9,
        category: ['Action, Adventure, Sci-Fi'],
      },
      {
        src: images[4] ,
        name: 'Doctor Strange',
        rating: 4.8,
        category: ['Action, Adventure, Fantasy'],
      },
      {
        src: images[5] ,
        name: 'Doctor Strange',
        rating: 4.8,
        category: ['Action, Adventure, Fantasy'],
      },
      {
        src: images[6] ,
        name: 'Fantastic Beasts...',
        rating: 4.7,
        category: ['Adventure', 'Family', 'Fantasy'],
      },
      {
        src: images[7] ,
        name: 'Fantastic Beasts...',
        rating: 4.7,
        category: ['Adventure', 'Family', 'Fantasy'],
      },
      {
        src: images[8] ,
        name: 'Finding Dory',
        rating: 4.7,
        category: ['Animation, Adventure, Comedy'],
      },
      {
        src: images[9] ,
        name: 'Moana',
        rating: 4.9,
        category: ['Action, Fantasy'],
      },
      {
        src: images[10] ,
        name: 'Ice Age: Collisio...',
        rating: 4.5,
        category: ['Adventure, Comedy'],
      },
      {
        src: images[11] ,
        name: 'Independence Day',
        rating: 3.9,
        category: ['Action, Sci-Fi'],
      },
      {
        src: images[12] ,
        name: 'Turtles',
        rating: 4.7,
        category: ['Adventure', 'Family', 'Fantasy'],
      },
      {
        src: images[13] ,
        name: 'Now you see me 2',
        rating: 4.4,
        category: ['Action, Adventure, Comedy'],
      },
      {
        src: images[14] ,
        name: 'Fantastic Beasts...',
        rating: 4.7,
        category: ['Adventure', 'Family', 'Fantasy'],
      },
      {
        src: images[15] ,
        name: 'The BFG',
        rating: 3.2,
        category: ['Adventure, Family, Fantasy'],
      },
    ],
};