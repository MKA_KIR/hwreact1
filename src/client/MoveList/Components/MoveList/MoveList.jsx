import React from 'react';
import { SortMenu } from '../../../../shared/components/SortMenu/';
import { DisplayMenu } from '../../../../shared/components/DisplayMenu/';
import { MoveCard } from '../../../../shared/components/MoveCard/';
import { Banner } from '../../../../shared/components/Banner/Components/Banner/';
import './moveList.css';
import {  moveList } from './move-list';
const {  moveCards, bannerRow } = moveList;
export const MoveList = (props) => {
  const cards = moveCards.map(item => <MoveCard {...item} />);
  const moveBeforeBanner = cards.slice(0, bannerRow * 4);
  const moveAfterBanner = cards.slice(bannerRow * 4);
  const {sortMenu} = props;
  return (
    <section className='move-list-section'>
      <div className='container'>
        <div className='move-list'>
          <div className='move-list-options'>
            <SortMenu menuList={sortMenu}/>
            <DisplayMenu/>
            <div className='move-list-content'>
              {moveBeforeBanner}
              <Banner/>
              {moveAfterBanner}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};