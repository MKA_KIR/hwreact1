import React from 'react';
import {Logo} from '../../../../shared/components/Logo';
import {AuthPanel} from '../../../../shared/components/AuthPanel';
import './Navbar.css';

export const Navbar = (props) => {
  const  {logo} = props;
  return(
    <nav className='navbar'>
      <div className='container'>
        <div className='navbar-row'>
          <Logo {...logo}/>
          <div className='serch'><i className="fas fa-search" /></div>;
          <AuthPanel />
        </div>
      </div>
    </nav>
  );
};
