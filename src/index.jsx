import React from 'react';
import { render } from 'react-dom';

import * as serviceWorker from './service-worker';
// import reportWebVitals from './report-web-vitals';
import { App } from "./app/componets/App";
// import '../node_modules/font-awesome/css/font-awesome.min.css';
// import 'font-awesome/css/font-awesome.css'
import "@fortawesome/fontawesome-free/css/all.css";

render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://cra.link/PWA
serviceWorker.unregister();

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
