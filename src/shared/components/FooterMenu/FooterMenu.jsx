import React from 'react';
import './footerMenu.css'

export const FooterMenu = (props) =>{
  const {footerMenu} = props;
  const menu = footerMenu.map(({href, text}) => <li className='footer-menu-item'><a className='footer-menu-link' href={href}>{text}</a></li>);
  return (
    <ul className='footer-menu'>
      {menu}
    </ul>
  );
};