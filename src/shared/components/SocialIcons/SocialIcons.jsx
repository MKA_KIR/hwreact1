import React from 'react';
import {SocialIcon} from './SocialIcon/';
import './socialIcons.css'

function importAll(r) {
  return r.keys().map(r);
}

const images = importAll(require.context('./images/', false, /\.(png|jpe?g|svg)$/));
const icons = [{link:'#', src: images[0]},{link:'#', src: images[1]},{link:'#', src: images[2]},
  {link:'#', src: images[3]},{link:'#', src: images[4]}];
export const SocialIcons = () =>{
  const iconsList = icons.map(item => <SocialIcon {...item}/>);

  return (
    <div className='socialIcons'>
      {iconsList}
    </div>
  );
};