import React from 'react';
import './socialIcon.css'

export const SocialIcon = (props) =>{
  const {link, src} = props;
  return (
    <a href={link} className='social-icon'><img src={src}/></a>
  );
};