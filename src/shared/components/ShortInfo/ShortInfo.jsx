import React from 'react';
import './shortInfo.css';

export const ShortInfo = (props) =>{
  const {rating} = props;
  return (
    <div className='rating'>
      <ShortInfo {...rating}/>
    </div>
  );
};