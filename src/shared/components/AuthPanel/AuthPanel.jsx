import React from 'react';
import {Button} from '../Button';
import './authPanel.css';

export const AuthPanel = (props) =>{
  const {authPanel} = props;
  return (
    <div className='auth-panel'>
      <Button text ="Sign in" type ='link'/>
      <Button text ='Sign up' type ='primary'/>
    </div>
  );
};