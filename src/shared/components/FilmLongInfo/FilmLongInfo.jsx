import React from 'react';
import './FilmlongInfo.css';
import {Button} from '../Button';

export const FilmLongInfo = (props) =>{
const {name, genres, duration, rating} = props;
  const genresItems = genres.map(item=><li className='genre-dew'>{item}</li>);
  const durationHours = Math.floor(duration/60);
  const durationMin = duration%60;
  const filmDuration = `${durationHours}h ${durationMin}m `;
  const stars = Array(Math.round(rating)).fill(<span className='star-dew'>&#9733;</span>);
  return (
    <div className='film-long-info'>
      <h2 className='text-h2'>{name}</h2>
      <div>
        <ul className='genre'>
          {genresItems}
          <li className='genre-dew'>|</li>
          {filmDuration}
        </ul>
      </div>
      <div className='header-footer'>
      <div className='star'>
        {stars}
        <span className='move-rating'>{rating}</span>
      </div>
        <div></div>
      <div className='long-info-button'>
        <Button text ="Watch Now" type ='primary'/>
        <Button text ='View Info' type ='viewinfo'/>
        <Button text ='+ Favorites' type ='link'/>
      </div>
      </div>
    </div>
  );
};