import React from 'react';
import './sortMenu.css';

export const SortMenu = (props) => {
  const {menuList} = props;
  const menuSort = menuList.map(item =><li className='menu-s'>{item}</li>);
  return(
    <div className='sort-menu'>
      <ul className='sort-container'>
        {menuSort}
      </ul>
    </div>
  );
};