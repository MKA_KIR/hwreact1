import React from 'react';
import './displayMenu.css';

export const DisplayMenu = (props) =>{
  const {displayMenu} = props;
  return (
    <div className='longInfo'>
      <div className='container'>
        {displayMenu}
      </div>
    </div>
  );
};