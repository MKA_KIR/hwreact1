import React from 'react';
import './Logo.css'
export const Logo = (props) =>{
  const {alt, src, firstWord, secondWord, type} = props;
  const className = (type==='color') ? 'logo logo-colored' : 'logo';
  const logoImg = (src) ? <img src = {src} alt = {alt} className='logo-img'/>: null;
  return(
    <a href='/' className={className}>
      {logoImg}
      <span className='logo-text logo-text-bold'>
        {firstWord}
      </span>
      <span className='logo-text'>
        {secondWord}
      </span>
    </a>
  );
};