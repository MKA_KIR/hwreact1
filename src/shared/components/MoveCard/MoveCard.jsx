import React from 'react';
import './moveCard.css';

export const MoveCard = (props) => {
  const { src, name, rating, category } = props;
  const categoryLinks = category.map(link => <a href='#' className='move-category'>{link}</a>);
  return (
    <div className="move-card">
      <img className='move-img' src={src}/>
      <div className='move-short-info'>
        <h4 className='move-name'>{name}</h4>
        <span className='move-rating'>{rating}</span>
      </div>
      <div className='move'>
        {categoryLinks}
      </div>
    </div>
  );
};
