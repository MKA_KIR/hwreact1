import React from 'react';
import {Button} from '../../../../../shared/components/Button';
import image from './images/MovieCollage@1X.png';
import image2 from './images/Footerbgcopy@1X.png';
import './banner.css';

export const Banner = (props) =>{
      return (
      <div className ='banner-image' style={{backgroundImage: `url(${image})`}}>
        <div className ='banner-image-second' style={{backgroundImage: `url(${image2})`}}>
      <div className='banner-container'>
      <h3 className='banner-text'>Receive information on the latest hit movies<br/>
      straight to your inbox.</h3>
      <div className='banner-button'>
      <Button text ="subscribe!" type ='primary'/>
      </div>
        </div>
        </div>
       </div>
      );
};
