import React from 'react';
// import 'bootstrap/dist/css/bootstrap.min.css';
// import '../../../shared/styles/scss/style.scss';
import {Header} from '../../../client/Header/Components/Header';
import {MoveList} from '../../../client/MoveList/Components/MoveList';
import {Footer} from '../../../client/Footer/Components/Footer/';
import './index.css';

const pageProps = {
    navbar: {
        logo: {
            firstWord: 'Movie',
            secondWord: 'Rise'
        },
    },
          filmLongInfo: {
            name: 'The Jungle Book',
            genres: ['Adventure', 'Drama', 'Family', 'Fantasy'],
            duration: 106,
            rating: 4.8
          },

    footer: {
         logo: {
           firstWord: 'Movie',
           secondWord: 'Rise'
    },
          footerMenu: [
            {href:'#', text: 'About'},
            {href:'#', text: 'Terms of Service'},
            {href:'#', text: 'Contact'}
            ]

  },
        sortMenu:[
          'trending',
          'top rated',
          'new arrivals',
          'trailers',
          'coming soon',
          'genre'
        ]
};

export const App = () => {
const {aboutUs, searchSection, moveList, moveCard, footer, sortMenu, ...header} = pageProps;
    return (
      <>
          <Header {...header}/>
          <div className="main-container">
              <MoveList sortMenu={sortMenu}/>
          </div>
          <Footer {...footer} />
      </>

    )
};
